package com.loginanimation

import android.animation.ObjectAnimator
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.animation.FastOutSlowInInterpolator
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreen : AppCompatActivity() {
    private var mDelayHandler: Handler? = null
    private val delay: Long = 3000
    private var objectAnimator: ObjectAnimator? = null

    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunnable, delay)

        objectAnimator = ObjectAnimator.ofFloat(logo, "alpha", 0f, 1f)
        objectAnimator!!.duration = delay / 2
        objectAnimator!!.interpolator = FastOutSlowInInterpolator()
        objectAnimator!!.repeatCount = 1
        objectAnimator!!.repeatMode = ObjectAnimator.REVERSE
        objectAnimator!!.start()
    }

    public override fun onDestroy() {
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }
}
